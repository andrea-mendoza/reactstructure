import React, { Component } from 'react'
import './Home.scss'
import '../../Common-blocks/Button.scss'


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: false};

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }


  showMessage(){
    return this.state.isToggleOn ? 'Hola Mundo' : ' ';
  }

  render() {
    return (
        <div className="home home__container">
          <button className="button home__button" onClick={this.handleClick}>Saludame</button>
        <p className="home__message">{this.showMessage()}</p>
      </div>
    );
  }
}