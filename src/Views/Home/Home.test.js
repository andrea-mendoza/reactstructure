import React from 'react';
import ReactDOM from 'react-dom';
import Home from './Home';
import { create } from 'react-test-renderer';
import { exportAllDeclaration } from '@babel/types';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Home />, div);
  ReactDOM.unmountComponentAtNode(div);
});


describe('Testing Home Componenet',() => {
    it('Show "hola React" en pantalla ', () => {
        let component = create(<Home/>);
        let instance = component.getInstance();
        let rootInstance = component.root;
        let button = rootInstance.findByType("button");

        button.props.onClick();
        expect(instance.showMessage()).toBe("Hola Mundo");
    });
})